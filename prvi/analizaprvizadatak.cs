﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication4
{
    public partial class Form1 : Form
    {
        double a, b, rez;
        public Form1()
        {
            InitializeComponent();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Application.Exit();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(!double.TryParse(textBox1.Text,out a)){
                MessageBox.Show("Krivi unos prvog operanda!");
            }
            if(!double.TryParse(textBox2.Text,out b)){
                MessageBox.Show("Krivi unos drugog operanda!");
            }
            rez=a+b;
            label3.Text=rez.ToString();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out a))
            {
                MessageBox.Show("Krivi unos prvog operanda!");
            }
            if (!double.TryParse(textBox2.Text, out b))
            {
                MessageBox.Show("Krivi unos drugog operanda!");
            }
            rez = a - b;
            label3.Text = rez.ToString();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out a))
            {
                MessageBox.Show("Krivi unos prvog operanda!");
            }
            if (!double.TryParse(textBox2.Text, out b))
            {
                MessageBox.Show("Krivi unos drugog operanda!");
            }
            rez = a / b;
            label3.Text = rez.ToString();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out a))
            {
                MessageBox.Show("Krivi unos prvog operanda!");
            }
            if (!double.TryParse(textBox2.Text, out b))
            {
                MessageBox.Show("Krivi unos drugog operanda!");
            }
            rez = a * b;
            label3.Text = rez.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out a))
            {
                MessageBox.Show("Krivi unos prvog operanda!");
            }

            rez = Math.Sin(a);
            label3.Text = rez.ToString();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out a))
            {
                MessageBox.Show("Krivi unos prvog operanda!");
            }
            
            rez =Math.Cos(a);
            label3.Text = rez.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out a))
            {
                MessageBox.Show("Krivi unos prvog operanda!");
            }
           
            rez = Math.Log10(a);
            label3.Text = rez.ToString();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out a))
            {
                MessageBox.Show("Krivi unos prvog operanda!");
            }
            
            rez = Math.Tan(a);
            label3.Text = rez.ToString();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out a))
            {
                MessageBox.Show("Krivi unos prvog operanda!");
            }
         

            rez = Math.Sqrt(a);
            label3.Text = rez.ToString();

        }
    }
}
